package bibliotekaApp;

import net.proteanit.sql.DbUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class uzytkownikApp {

    static Color drzwi = Color.decode("#00A000");
    static Color ksiazka = Color.decode("#671832");
    static Color napis = Color.decode("#AEA725");
    private static JTable tableWyp;
    private static JTable tableRez;
    private static JTable tableZarez;
    private static JTable tableLista;
    private static JTable tableInfo;
    private static JTable tableZap;
    private static JPasswordField starehaslo;
    private static JPasswordField nowehaslo;
    private static JPasswordField powtorzhaslo;
    private static JPasswordField haslo;
    private static JTextField ksiazkawyp;
    private static JTextField ksiazkarez;
    private static JTextField pin;
    private static JTextField login;
    private static JTextField imie;
    private static JTextField nazwisko;
    private static JTextField ulica;
    private static JTextField miasto;
    private static JTextField kodpocztowy;
    private static JTextField telefon;
    private static JTextField email;
    private static JTextField loginPrzyp;
    private static JTextField nazwiskoPrzyp;
    private static JTextPane txtpnGodzinyOtwarcia;
    private static JTextPane txtautorzy;
    private static JTextPane txtregulamin;
    private static String status;
    static Connection connection = null;
    private JFrame f = new JFrame();
    private JFrame log = new JFrame();
    private List<?> lista;
    private String z;


    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    uzytkownikApp window = new uzytkownikApp();
                    window.f.setVisible(false);
                    window.log.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public uzytkownikApp() {
        initialize();
        connection = databaseConnection.dbCon();
    }

    private void initialize() {

        //background


        try {
            f.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("background.jpg"))))); // �cie�ka do pliku .jpg (podw�jne backslashe)
        } catch (IOException e) {
            e.printStackTrace();
        }
        f.pack();
        f.setLocationRelativeTo(null);
        f.setTitle("Biblioteka / Wersja użytkownika");
        f.setSize(new Dimension(1111, 422));
        f.setVisible(true);

        String numbers = "123456789";
        Random random = new Random();

        int j = 0;
        int y = 0;
        int b = 0;
        String code = "";

        y = random.nextInt(2);
        if (y == 0) {
            b = 3;
        } else if (y == 1) {
            b = 3;
        } else {
            b = 3;
        }

        for (j = 0; j < b; j++) {
            code = code + numbers.charAt(random.nextInt(9));
        }

        String pinpas = code;

        JFrame zapomnialem = new JFrame();
        zapomnialem.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        zapomnialem.setTitle("Biblioteka / Zapomniałem hasła");
        zapomnialem.setSize(284, 203);
        zapomnialem.setLocationRelativeTo(null);
        zapomnialem.setLayout(null);

        loginPrzyp = new JTextField();
        loginPrzyp.setBounds(145, 30, 86, 20);
        zapomnialem.add(loginPrzyp);
        loginPrzyp.setColumns(10);

        nazwiskoPrzyp = new JTextField();
        nazwiskoPrzyp.setColumns(10);
        nazwiskoPrzyp.setBounds(145, 61, 86, 20);
        zapomnialem.add(nazwiskoPrzyp);

        JLabel lblNazwiskoPrzyp = new JLabel("Nazwisko:");
        lblNazwiskoPrzyp.setBounds(46, 64, 89, 14);
        zapomnialem.add(lblNazwiskoPrzyp);

        JLabel lblLogPrzyp = new JLabel("Login:");
        lblLogPrzyp.setBounds(46, 33, 89, 14);
        zapomnialem.add(lblLogPrzyp);

        tableZap = new JTable() {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {
                return false;
            }

            ;
        };

        JScrollPane scrollZap = new JScrollPane();
        scrollZap.setBounds(10, 11, 258, 79);
        scrollZap.setViewportView(tableZap);
        zapomnialem.add(scrollZap);
        scrollZap.setVisible(false);

        JButton zakoncz = new JButton("Zakończ");
        zakoncz.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                zapomnialem.dispose();
            }
        });
        zakoncz.setBounds(60, 113, 143, 23);
        zapomnialem.add(zakoncz);
        zakoncz.setVisible(false);

        JButton przypHaslo = new JButton("Odzyskaj hasło");
        przypHaslo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String query = "SELECT Login, Haslo FROM UZYTKOWNIK WHERE Login = '" + loginPrzyp.getText() + "' AND Nazwisko = '" + nazwiskoPrzyp.getText() + "'";
                    PreparedStatement pst = connection.prepareStatement(query);
                    ResultSet rs = pst.executeQuery();
                    tableZap.setModel(DbUtils.resultSetToTableModel(rs));

                    loginPrzyp.setVisible(false);
                    nazwiskoPrzyp.setVisible(false);
                    lblLogPrzyp.setVisible(false);
                    lblNazwiskoPrzyp.setVisible(false);
                    scrollZap.setVisible(true);
                    zakoncz.setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Błąd podczas przywracania hasła: spróbuj ponownie.", "Biblioteka / Zapomniałem hasła", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        przypHaslo.setBounds(60, 113, 143, 23);
        zapomnialem.add(przypHaslo);

        JFrame rejestracja = new JFrame();
        rejestracja.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        rejestracja.setTitle("Biblioteka / Rejestracja");
        rejestracja.setSize(284, 370);
        rejestracja.setLocationRelativeTo(null);
        rejestracja.setLayout(null);

        imie = new JTextField();
        imie.setBounds(145, 30, 86, 20);
        rejestracja.add(imie);
        imie.setColumns(10);

        nazwisko = new JTextField();
        nazwisko.setColumns(10);
        nazwisko.setBounds(145, 61, 86, 20);
        rejestracja.add(nazwisko);

        ulica = new JTextField();
        ulica.setColumns(10);
        ulica.setBounds(145, 92, 86, 20);
        rejestracja.add(ulica);

        miasto = new JTextField();
        miasto.setColumns(10);
        miasto.setBounds(145, 123, 86, 20);
        rejestracja.add(miasto);

        kodpocztowy = new JTextField();
        kodpocztowy.setColumns(10);
        kodpocztowy.setBounds(145, 154, 86, 20);
        rejestracja.add(kodpocztowy);

        telefon = new JTextField();
        telefon.setColumns(10);
        telefon.setBounds(145, 185, 86, 20);
        rejestracja.add(telefon);

        email = new JTextField();
        email.setColumns(10);
        email.setBounds(145, 216, 86, 20);
        rejestracja.add(email);

        JLabel lblimie = new JLabel("Imię");
        lblimie.setBounds(46, 33, 68, 14);
        rejestracja.add(lblimie);

        JLabel lblnazwisko = new JLabel("Nazwisko");
        lblnazwisko.setBounds(46, 64, 68, 14);
        rejestracja.add(lblnazwisko);

        JLabel lblulica = new JLabel("Ulica");
        lblulica.setBounds(46, 95, 68, 14);
        rejestracja.add(lblulica);

        JLabel lblmiasto = new JLabel("Miasto");
        lblmiasto.setBounds(46, 126, 68, 14);
        rejestracja.add(lblmiasto);

        JLabel lbltelefon = new JLabel("Telefon");
        lbltelefon.setBounds(46, 188, 68, 14);
        rejestracja.add(lbltelefon);

        JLabel lblemail = new JLabel("Email");
        lblemail.setBounds(46, 219, 68, 14);
        rejestracja.add(lblemail);

        JLabel lblkodpocztowy = new JLabel("Kod pocztowy");
        lblkodpocztowy.setBounds(46, 157, 89, 14);
        rejestracja.add(lblkodpocztowy);

        JLabel lblinfo1 = new JLabel("Prosimy zapisać (np. w telefonie) wygenerowany");
        lblinfo1.setBounds(10, 244, 258, 14);
        rejestracja.add(lblinfo1);
        lblinfo1.setVisible(false);

        JLabel lblinfo2 = new JLabel("Login oraz Hasło!");
        lblinfo2.setBounds(10, 257, 258, 14);
        rejestracja.add(lblinfo2);
        lblinfo2.setVisible(false);

        JLabel twojLogin = new JLabel("Twój login");
        twojLogin.setBounds(46, 95, 68, 14);
        rejestracja.add(twojLogin);
        twojLogin.setVisible(false);

        JLabel twojeHaslo = new JLabel("Twoje hasło");
        twojeHaslo.setBounds(46, 126, 68, 14);
        rejestracja.add(twojeHaslo);
        twojeHaslo.setVisible(false);

        JButton btnZakoczRejestracj = new JButton("Zakończ rejestrację");
        btnZakoczRejestracj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                rejestracja.dispose();
            }
        });
        btnZakoczRejestracj.setBounds(46, 290, 185, 23);
        rejestracja.add(btnZakoczRejestracj);
        btnZakoczRejestracj.setVisible(false);

        JButton btnWygenerujLoginI = new JButton("Wygeneruj login i hasło");
        btnWygenerujLoginI.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                String fullalphabet = alphabet + alphabet.toLowerCase() + "123456789";
                Random random = new Random();

                int i = 0;
                int x = 0;
                int a = 0;
                String code1 = "";
                String code2 = "";

                x = random.nextInt(2);
                if (x == 0) {
                    a = 6;
                } else if (x == 1) {
                    a = 7;
                } else {
                    a = 8;
                }

                for (i = 0; i < a; i++) {
                    code1 = code1 + fullalphabet.charAt(random.nextInt(60));
                    code2 = code2 + fullalphabet.charAt(random.nextInt(60));
                }
                i = 0;

                String login = code1;
                String haslo = code2;

                try {
                    String query = "INSERT INTO UZYTKOWNIK (Login,Haslo,Imie,Nazwisko,Ulica,Miasto,Kod_pocztowy,Telefon,Email) VALUES (?,?,?,?,?,?,?,?,?)";
                    PreparedStatement pst = connection.prepareStatement(query);
                    pst.setString(1, login);
                    pst.setString(2, haslo);
                    pst.setString(3, imie.getText());
                    pst.setString(4, nazwisko.getText());
                    pst.setString(5, ulica.getText());
                    pst.setString(6, miasto.getText());
                    pst.setString(7, kodpocztowy.getText());
                    pst.setString(8, telefon.getText());
                    pst.setString(9, email.getText());

                    pst.execute();

                    pst.close();

                    imie.setText("");
                    nazwisko.setText("");
                    ulica.setText("");
                    miasto.setText("");
                    kodpocztowy.setText("");
                    telefon.setText("");
                    email.setText("");

                    imie.setVisible(false);
                    nazwisko.setVisible(false);
                    kodpocztowy.setVisible(false);
                    telefon.setVisible(false);
                    email.setVisible(false);
                    lblimie.setVisible(false);
                    lblnazwisko.setVisible(false);
                    lblulica.setVisible(false);
                    lblmiasto.setVisible(false);
                    lblkodpocztowy.setVisible(false);
                    lbltelefon.setVisible(false);
                    lblemail.setVisible(false);
                    btnWygenerujLoginI.setVisible(false);

                    twojLogin.setVisible(true);
                    twojeHaslo.setVisible(true);
                    ulica.setText(login);
                    miasto.setText(haslo);
                    lblinfo1.setVisible(true);
                    lblinfo2.setVisible(true);
                    btnZakoczRejestracj.setVisible(true);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnWygenerujLoginI.setBounds(46, 268, 185, 23);
        rejestracja.add(btnWygenerujLoginI);

        log.setSize(394, 302);
        log.setTitle("Biblioteka / Logowanie");
        log.setLayout(null);
        log.setLocationRelativeTo(null);

        JLabel lblLogWpro = new JLabel("Login:");
        lblLogWpro.setBounds(68, 68, 46, 14);
        log.add(lblLogWpro);

        JLabel lblHasWpro = new JLabel("Hasło:");
        lblHasWpro.setBounds(68, 93, 46, 14);
        log.add(lblHasWpro);

        haslo = new JPasswordField();
        haslo.setBounds(157, 90, 123, 20);
        log.add(haslo);
        haslo.setColumns(10);

        pin = new JTextField();
        pin.setBounds(157, 115, 61, 20);
        log.add(pin);
        pin.setColumns(10);

        JLabel lblNewLabel = new JLabel("Przepisz kod:");
        lblNewLabel.setBounds(68, 118, 79, 14);
        log.add(lblNewLabel);

        login = new JTextField();
        login.setBounds(157, 65, 123, 20);
        log.add(login);
        login.setColumns(10);

        JLabel lblPin = new JLabel("");
        lblPin.setBounds(228, 118, 71, 14);
        log.add(lblPin);
        lblPin.setText(pinpas);

        JButton btnZapomniales = new JButton("Zapomniałeś hasła?");
        btnZapomniales.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                zapomnialem.setVisible(true);
            }
        });
        btnZapomniales.setBounds(24, 146, 151, 23);
        log.add(btnZapomniales);

        JButton btnZaloguj = new JButton("Zaloguj");
        btnZaloguj.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String password = String.copyValueOf(haslo.getPassword());
                String pinget = String.valueOf(lblPin.getText());
                String pincheck = String.valueOf(pin.getText());
                try {
                    String query = "SELECT Login FROM UZYTKOWNIK WHERE Login = '" + login.getText() + "' AND Haslo = '" + password + "'";

                    PreparedStatement pst = connection.prepareStatement(query);

                    ResultSet rs = pst.executeQuery();
                    lista = DbUtils.resultSetToNestedList(rs);
                    z = lista.toString();

                    if (z == "[]") {
                        JOptionPane.showMessageDialog(null, "Błędny login lub hasło", "Biblioteka / Logowanie", JOptionPane.ERROR_MESSAGE);
                    } else {
                        if (pinget.equals(pincheck)) {
                            JOptionPane.showMessageDialog(null, "Zalogowano pomyślnie!", "Biblioteka / Logowanie", JOptionPane.INFORMATION_MESSAGE);
                            status = login.getText();
                            f.setVisible(true);
                            log.setVisible(false);
                        } else {
                            JOptionPane.showMessageDialog(null, "Błędny PIN: spróbuj ponownie.", "Biblioteka / Logowanie", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Błąd podczas logowania: spróbuj ponownie.", "Biblioteka / Logowanie", JOptionPane.ERROR_MESSAGE);
                }

            }

        });
        btnZaloguj.setBounds(210, 146, 111, 23);
        log.add(btnZaloguj);

        JButton btnRejestracja = new JButton("Rejestracja");
        btnRejestracja.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                rejestracja.setVisible(true);
            }
        });
        btnRejestracja.setBounds(210, 180, 111, 23);
        log.add(btnRejestracja);

        //buttons

        JButton bt1 = new JButton("WYPOŻYCZONE");
        bt1.setFont(new Font("Arial", Font.BOLD, 12));
        bt1.setVisible(true);
        bt1.setBounds(250, 115, 125, 20);
        f.getContentPane().add(bt1);

        JButton bt2 = new JButton("ZAREZERWOWANE");
        bt2.setFont(new Font("Arial", Font.BOLD, 12));
        bt2.setVisible(true);
        bt2.setBounds(380, 115, 145, 20);
        f.getContentPane().add(bt2);

        JButton bt3 = new JButton("WYPOŻYCZ");
        bt3.setFont(new Font("Arial", Font.BOLD, 12));
        bt3.setVisible(true);
        bt3.setBounds(302, 152, 110, 30);
        f.getContentPane().add(bt3);

        JButton bt4 = new JButton("REZERWUJ");
        bt4.setFont(new Font("Arial", Font.BOLD, 12));
        bt4.setVisible(true);
        bt4.setBounds(420, 152, 110, 30);
        f.getContentPane().add(bt4);

        JButton bt5 = new JButton("LISTA DOSTĘPNYCH KSIĄŻEK");
        bt5.setFont(new Font("Arial", Font.BOLD, 12));
        bt5.setVisible(true);
        bt5.setBounds(302, 198, 220, 35);
        f.getContentPane().add(bt5);

        JButton bt6 = new JButton("REGULAMIN");
        bt6.setFont(new Font("Arial", Font.BOLD, 12));
        bt6.setVisible(true);
        bt6.setBounds(330, 245, 220, 46);
        f.getContentPane().add(bt6);

        JButton bte = new JButton("WYJŚCIE");
        bte.setFont(new Font("Arial", Font.BOLD, 20));
        bte.setVisible(true);
        bte.setBounds(15, 65, 205, 50);
        f.getContentPane().add(bte);
        bte.setBackground(drzwi);
        bte.setForeground(Color.WHITE);

        JButton btb = new JButton("Godziny otwarcia");
        btb.setFont(new Font("Arial", Font.BOLD, 12));
        btb.setVisible(true);
        btb.setBounds(950, 110, 140, 70);
        f.getContentPane().add(btb);
        btb.setBackground(ksiazka);
        btb.setForeground(napis);

        JButton bta = new JButton("O Autorach");
        bta.setFont(new Font("Arial", Font.BOLD, 12));
        bta.setVisible(true);
        bta.setBounds(950, 200, 140, 70);
        f.getContentPane().add(bta);
        bta.setBackground(ksiazka);
        bta.setForeground(napis);

        JButton bti = new JButton("Informacje o koncie");
        bti.setFont(new Font("Arial", Font.BOLD, 12));
        bti.setVisible(true);
        bti.setBounds(920, 10, 160, 20);
        f.getContentPane().add(bti);

        JButton zmienhaslo = new JButton("Zmień hasło");
        zmienhaslo.setFont(new Font("Arial", Font.BOLD, 12));
        zmienhaslo.setVisible(true);
        zmienhaslo.setBounds(920, 40, 160, 20);
        f.getContentPane().add(zmienhaslo);

        // tables

        tableWyp = new JTable() {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {
                return false;
            }

            ;
        };

        tableRez = new JTable() {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {
                return false;
            }

            ;
        };

        tableZarez = new JTable() {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {
                return false;
            }

            ;
        };

        tableLista = new JTable() {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {
                return false;
            }

            ;
        };

        tableInfo = new JTable() {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column) {
                return false;
            }

            ;
        };


        txtpnGodzinyOtwarcia = new JTextPane();
        txtpnGodzinyOtwarcia.setText("Godziny otwarcia:");
        txtpnGodzinyOtwarcia.setEditable(false);

        txtregulamin = new JTextPane();
        txtregulamin.setText("Regulamin:");
        txtregulamin.setEditable(false);

        txtautorzy = new JTextPane();
        txtautorzy.setText("Autorzy: \n\nDyrda Mateusz: programista, grafik \nPankowski Rafał: tester, dokumentalista \nMateusz Porębski: programista baz danych, dokumentalista \nZarzycki Paweł: analityk, projektant \n\n\nProgram został wykonany przez studentów Politechniki Koszali�skiej, studia I stopnia, kierunek Informatyka, semestr 6, specjalność ITO, w ramach projektu na zaliczenie przedmiotu Projekt zespołowy - Projektowe.");
        txtautorzy.setEditable(false);

        // opening new windows

        JScrollPane scrollWyp = new JScrollPane();
        scrollWyp.setSize(600, 400);
        scrollWyp.setViewportView(tableWyp);

        JScrollPane scrollRez = new JScrollPane();
        scrollRez.setSize(600, 400);
        scrollRez.setViewportView(tableRez);

        JScrollPane scrollZarez = new JScrollPane();
        scrollZarez.setSize(600, 400);
        scrollZarez.setViewportView(tableZarez);

        JScrollPane scrollLista = new JScrollPane();
        scrollLista.setSize(600, 400);
        scrollLista.setViewportView(tableLista);

        JScrollPane scrollInfo = new JScrollPane();
        scrollInfo.setSize(1400, 50);
        scrollInfo.setViewportView(tableInfo);

        JFrame wypozyczone = new JFrame();
        wypozyczone.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        wypozyczone.setTitle("Wypożyczone książki");
        wypozyczone.setSize(600, 400);
        wypozyczone.setLocationRelativeTo(null);
        wypozyczone.getContentPane().add(scrollWyp);

        JFrame zarezerwowane = new JFrame();
        zarezerwowane.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        zarezerwowane.setTitle("Zarezerwowane książki");
        zarezerwowane.setSize(600, 400);
        zarezerwowane.setLocationRelativeTo(null);
        zarezerwowane.getContentPane().add(scrollZarez);

        JFrame rezerwuj = new JFrame();
        rezerwuj.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        rezerwuj.setTitle("Zarezerwuj książkę");
        rezerwuj.setSize(450, 215);
        rezerwuj.setLocationRelativeTo(null);
        rezerwuj.getContentPane().setLayout(null);

        ksiazkarez = new JTextField();
        ksiazkarez.setBounds(177, 59, 130, 20);
        rezerwuj.getContentPane().add(ksiazkarez);
        ksiazkarez.setColumns(10);

        JButton btnrez = new JButton("Zarezerwuj");
        btnrez.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                Calendar cal = Calendar.getInstance();
                String termrez = String.valueOf(sdf.format(cal.getTime()));
                try {
                    String query = "INSERT INTO REZERWACJA (Id_ksiazki, Id_uzytkownika, Data_rezerwacji) SELECT KSIAZKI.Id_ksiazki, UZYTKOWNIK.Id_uzytkownika, '" + termrez + "' FROM KSIAZKI, UZYTKOWNIK WHERE KSIAZKI.Tytul = '" + ksiazkarez.getText() + "' AND UZYTKOWNIK.Login = '" + status + "'";
                    PreparedStatement pst = connection.prepareStatement(query);

                    pst.execute();

                    JOptionPane.showMessageDialog(null, "Pomyślnie zarezerwowano książkę!", "Zarezerwuj książkę", JOptionPane.INFORMATION_MESSAGE);

                    pst.close();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Błąd podczas rezerwowania: spróbuj ponownie.", "Zarezerwuj książkę", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnrez.setBounds(152, 122, 110, 23);
        rezerwuj.getContentPane().add(btnrez);

        JLabel lblTytylRez = new JLabel("Podaj tytuł książki:");
        lblTytylRez.setBounds(57, 62, 110, 14);
        rezerwuj.getContentPane().add(lblTytylRez);


        JFrame wypozycz = new JFrame();
        wypozycz.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        wypozycz.setTitle("Wypożycz książkę");
        wypozycz.setSize(450, 215);
        wypozycz.setLocationRelativeTo(null);
        wypozycz.getContentPane().setLayout(null);

        ksiazkawyp = new JTextField();
        ksiazkawyp.setBounds(177, 59, 130, 20);
        wypozycz.getContentPane().add(ksiazkawyp);
        ksiazkawyp.setColumns(10);

        JButton btnwyp = new JButton("Wypożycz");
        btnwyp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                Calendar cal = Calendar.getInstance();
                String termwyp = String.valueOf(sdf.format(cal.getTime()));
                cal.add(Calendar.DAY_OF_MONTH, 30);
                String termzwr = String.valueOf(sdf.format(cal.getTime()));
                try {
                    String query = "INSERT INTO WYPOZYCZENIE (Id_ksiazki, Id_uzytkownika, Termin_wypozyczenia, Termin_oddania) SELECT KSIAZKI.Id_ksiazki, UZYTKOWNIK.Id_uzytkownika, '" + termwyp + "', '" + termzwr + "' FROM KSIAZKI, UZYTKOWNIK WHERE KSIAZKI.Tytul = '" + ksiazkawyp.getText() + "' AND UZYTKOWNIK.Login = '" + status + "'";
                    PreparedStatement pst = connection.prepareStatement(query);

                    pst.execute();

                    JOptionPane.showMessageDialog(null, "Pomyślnie wypo�yczono książkę!", "Wypożycz książkę", JOptionPane.INFORMATION_MESSAGE);

                    pst.close();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Błąd podczas wypożyczania: spróbuj ponownie.", "Wypożycz książkę", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnwyp.setBounds(152, 122, 110, 23);
        wypozycz.getContentPane().add(btnwyp);

        JLabel lblTytylWyp = new JLabel("Podaj tytuł książki:");
        lblTytylWyp.setBounds(57, 62, 110, 14);
        wypozycz.getContentPane().add(lblTytylWyp);

        JFrame lista = new JFrame();
        lista.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        lista.setTitle("Lista dostępnych książek");
        lista.setSize(600, 400);
        lista.setLocationRelativeTo(null);
        lista.getContentPane().add(scrollLista);

        JFrame regulamin = new JFrame();
        regulamin.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        regulamin.setTitle("Regulamin");
        regulamin.setSize(600, 400);
        regulamin.setLocationRelativeTo(null);
        regulamin.getContentPane().add(txtregulamin);

        JFrame godziny = new JFrame();
        godziny.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        godziny.setTitle("Godziny otwarcia");
        godziny.setSize(600, 400);
        godziny.setLocationRelativeTo(null);
        godziny.getContentPane().add(txtpnGodzinyOtwarcia);

        JFrame autorzy = new JFrame();
        autorzy.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        autorzy.setTitle("O Autorach");
        autorzy.setSize(600, 400);
        autorzy.setLocationRelativeTo(null);
        autorzy.getContentPane().add(txtautorzy);

        JFrame zmiana = new JFrame();
        zmiana.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        zmiana.setTitle("Zmiana hasła");
        zmiana.setSize(450, 300);
        zmiana.setLocationRelativeTo(null);
        zmiana.getContentPane().setLayout(null);

        JLabel lblstarehaslo = new JLabel("Stare hasło:");
        lblstarehaslo.setBounds(81, 62, 76, 14);
        zmiana.getContentPane().add(lblstarehaslo);

        starehaslo = new JPasswordField();
        starehaslo.setBounds(177, 59, 130, 20);
        zmiana.getContentPane().add(starehaslo);
        starehaslo.setColumns(10);

        JLabel lblnowehaslo = new JLabel("Nowe hasło:");
        lblnowehaslo.setBounds(81, 93, 76, 14);
        zmiana.getContentPane().add(lblnowehaslo);

        nowehaslo = new JPasswordField();
        nowehaslo.setColumns(10);
        nowehaslo.setBounds(177, 90, 130, 20);
        zmiana.getContentPane().add(nowehaslo);

        powtorzhaslo = new JPasswordField();
        powtorzhaslo.setColumns(10);
        powtorzhaslo.setBounds(177, 121, 130, 20);
        zmiana.getContentPane().add(powtorzhaslo);

        JLabel lblpowtorzhaslo = new JLabel("Powtórz hasło:");
        lblpowtorzhaslo.setBounds(81, 124, 86, 14);
        zmiana.getContentPane().add(lblpowtorzhaslo);

        JButton zmianahasla = new JButton("Zmień hasło");
        zmianahasla.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String stare = String.copyValueOf(starehaslo.getPassword());
                String nowe = String.copyValueOf(nowehaslo.getPassword());
                String powtorz = String.copyValueOf(powtorzhaslo.getPassword());

                if (nowe.equals(powtorz)) {
                    try {
                        String query = "UPDATE UZYTKOWNIK SET Haslo = '" + nowe + "' WHERE Login = '" + status + "' AND Haslo = '" + stare + "'";
                        PreparedStatement pst = connection.prepareStatement(query);

                        pst.execute();

                        JOptionPane.showMessageDialog(null, "Hasło zostało zmienione!", "Zmiana hasła", JOptionPane.INFORMATION_MESSAGE);


                        pst.close();

                        nowehaslo.setText("");
                        powtorzhaslo.setText("");
                        starehaslo.setText("");

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Błąd podczas zmiany hasła: spróbuj ponownie.", "Zmiana hasła", JOptionPane.ERROR_MESSAGE);
                    }

                }
                zmiana.dispose();
            }
        });
        zmianahasla.setBounds(152, 193, 110, 23);
        zmiana.getContentPane().add(zmianahasla);

        JFrame info = new JFrame();
        info.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        info.setTitle("Informacje o koncie");
        info.setSize(1400, 100);
        info.setLocationRelativeTo(null);
        info.getContentPane().add(scrollInfo);

        // action listeners
        bt1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                wypozyczone.setVisible(true);
                try {
                    String query = "SELECT KSIAZKI.Tytul, KSIAZKI.Autor, WYPOZYCZENIE.Termin_wypozyczenia AS Wypo�yczono, WYPOZYCZENIE.Termin_oddania AS Odda� FROM KSIAZKI, WYPOZYCZENIE, UZYTKOWNIK WHERE UZYTKOWNIK.Login = '" + status + "' AND WYPOZYCZENIE.Id_uzytkownika =  UZYTKOWNIK.Id_uzytkownika AND KSIAZKI.Id_ksiazki = WYPOZYCZENIE.Id_ksiazki ORDER BY WYPOZYCZENIE.Termin_oddania";
                    PreparedStatement pst = connection.prepareStatement(query);
                    ResultSet rs = pst.executeQuery();
                    tableWyp.setModel(DbUtils.resultSetToTableModel(rs));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Błąd podczas wyświetlania: spróbuj ponownie.", "Wypożyczone książki", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        bt2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                zarezerwowane.setVisible(true);
                try {
                    String query = "SELECT KSIAZKI.Tytul, KSIAZKI.Autor, REZERWACJA.Data_rezerwacji AS Zarezerwowano FROM KSIAZKI, REZERWACJA, UZYTKOWNIK WHERE UZYTKOWNIK.Login = '" + status + "' AND REZERWACJA.Id_uzytkownika =  UZYTKOWNIK.Id_uzytkownika AND KSIAZKI.Id_ksiazki = REZERWACJA.Id_ksiazki ORDER BY REZERWACJA.Data_rezerwacji";
                    PreparedStatement pst = connection.prepareStatement(query);
                    ResultSet rs = pst.executeQuery();
                    tableZarez.setModel(DbUtils.resultSetToTableModel(rs));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Błąd podczas wyświetlania: spróbuj ponownie.", "Zarezerwowane książki", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        bt3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                wypozycz.setVisible(true);
            }
        });

        bt4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                rezerwuj.setVisible(true);
            }
        });

        bt5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                lista.setVisible(true);
                try {
                    String query = "SELECT Tytul, Gatunek, Autor, Rok_wydania, Sztuk FROM KSIAZKI";
                    PreparedStatement pst = connection.prepareStatement(query);
                    ResultSet rs = pst.executeQuery();
                    tableLista.setModel(DbUtils.resultSetToTableModel(rs));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Błąd podczas wyświetlania: spróbuj ponownie.", "Lista dostępnych książek", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        bt6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                regulamin.setVisible(true);
            }
        });

        btb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                godziny.setVisible(true);
            }
        });

        bta.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                autorzy.setVisible(true);
            }
        });

        bti.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                info.setVisible(true);
                try {
                    String query = "SELECT Login, Imie, Nazwisko, Ulica, Miasto, Kod_pocztowy, Telefon, Email FROM UZYTKOWNIK WHERE Login = '" + status + "'";
                    PreparedStatement pst = connection.prepareStatement(query);
                    ResultSet rs = pst.executeQuery();
                    tableInfo.setModel(DbUtils.resultSetToTableModel(rs));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Błąd podczas wyświetlania: spróbuj ponownie.", "Informacje o koncie", JOptionPane.ERROR_MESSAGE);
                }

            }
        });

        zmienhaslo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                zmiana.setVisible(true);
            }
        });

        bte.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });


    }


}