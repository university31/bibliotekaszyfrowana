package bibliotekaApp;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;

public class databaseConnection {
    Connection conn = null;

    public static Connection dbCon() {
        try {
            Class.forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection("jdbc:sqlite:sqlite\\db\\Biblioteka.db");
            return conn;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
}
